# 1. Create an activity folder and an activity.py inside of it.

# 2. Mimic the given terminal output by creating the following "list".

	#Activity No. 1 
	# a. Create a list named "numbers" that will contain five(5) integer values.
numbers = [1, 2, 3.0, 22, 45]

	# b. Create a list named "meals" that will contain three(3) string values.
meals = ["Biryani", "Thali", "SPAM!"]

# Activity No. 3
	# 3. Using the common "list literals and operators", mimic the given terminal output.


		# a. Create a "messages" list and insert a string value "Hi!" four times using the repeat operator.
message = ["Hi!"]
messages = message * 4

	# b. Perform the following operations using the "numbers" list.

		# b.1. Print the length of the "numbers" list
print(len(numbers))

		# b.2. Removed the last item on the "numbers" list.
numbers.pop()

		# b.3. Reverse the sorting order of the "numbers" list items.
numbers.reverse()

	# c. Perform the following operations using the "meals" list.

		# c.1. Using the indexing operation, print the item "SPAM!" from the "meals" list.
print(meals[2])

		# c.2. Using the negative indexing operation, print the item "Spam" from the "L" list.
print(meals[-1])

		# c.3. Reassign the item "Spam" with "eggs" and print the modified "meals" list.
meals[2] = "Eggs"
print(meals)

		# c.4. Add another list item with a value of "bacon" and print the modified "meals" list.
meals.append("Bacon")
print(meals)

		# c.5. Arrange your list items using the sort method and print the modified "meals" list.
meals.sort()
print(meals)

	# Activity No. 4 (Modified this one)
	# 4. Create a dictionary named "recipe" with an initial key of "eggs" and a value of 3.
recipe = {
	"eggs" : 3
}
		# Activity No. 5
		# a. Add the following keys and values in the recipe: 
			# "spam":2, "ham":1, "brunch": bacon
recipe["spam"] = 2
recipe["ham"] = 1
recipe["brunch"] = "bacon"

		# and print the "recipe" to view changes. 
print(recipe)

		# b. Update the "ham" key value with the following:
			# ["grill", "bake", "fry"]
recipe["ham"] = {"grill", "bake", "fry"}

		# and print the "recipe" to view changes.
print(recipe)

		# c. Delete the "eggs" key in the dictionary and print the "recipe" to view changes.
del recipe["eggs"]

	# Activity No.6
	# 5. Using the provided "bob" dictionary, access and print the following key values in the terminal.

bob = 	{'name': {'first': 'Bob', 'last': 'Smith'},
		 'age': 42,
		 'job': ['software', 'writing'],
		 'pay': (40000, 50000)}

		# a. Print the "name" values.
print(bob["name"])

		# b. Print the "Smith" value in the "name" key.
print(bob["name"]["last"])

		# c. Print the "5000" value in the "pay" key.
print(bob["pay"][1])

# 6. Using the provided "numeric" tuple, access and print the following items in the terminal.4
# a. Print the item equal to "3.0"
print(numbers[1])

# b. Print the item equal to "22"
print(numbers[0])